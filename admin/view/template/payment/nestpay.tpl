<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
	  <div class="buttons"><a onclick="$('#form-nestpay-hosted').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
       
        
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
        <small><a href="http://optininja.com">OptiNinja.com</a></small>
      </div>
      <div class="panel-body">
        <form action="<?php //echo $action; ?>" method="POST" enctype="multipart/form-data" id="form-nestpay-hosted" class="form-horizontal">

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-nestpay_url">URL</label>
            <div class="col-sm-10">

              <input type="text" name="nestpay_url" value="<?php echo $nestpay_url; ?>" placeholder="" id="input-nestpay_url" class="form-control" />

            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-nestpay_merchant_id">MerchantID</label>
            <div class="col-sm-10">

              <input type="text" name="nestpay_merchant_id" value="<?php echo $nestpay_merchant_id; ?>" placeholder="" id="input-nestpay_merchant_id" class="form-control" />

            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-nestpay_storekey">StoreKey</label>
            <div class="col-sm-10">

              <input type="password" name="nestpay_storekey" value="<?php echo $nestpay_storekey; ?>" placeholder="" id="input-nestpay_storekey" class="form-control" />

            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-nestpay_api">Licenca modula</label>
            <div class="col-sm-10">

              <input type="text" name="nestpay_api" value="<?php echo $nestpay_api; ?>" placeholder="" id="input-nestpay_api" class="form-control" />

            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-nestpay_transaction_email">Admin transaction email</label>
            <div class="col-sm-10">

              <input type="text" name="nestpay_transaction_email" value="<?php echo $nestpay_transaction_email; ?>" placeholder="" id="input-nestpay_transaction_email" class="form-control" />

            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-nestpay_transaction_type">Tip transakcije</label>
            <div class="col-sm-10">

              <input type="radio" name="nestpay_transaction_type" value="Auth" placeholder="" id="input-nestpay_transaction_type" class="form-control" <?php if($nestpay_transaction_type == "Auth" ) { echo "checked"; } ?> /> Auth
              <input type="radio" name="nestpay_transaction_type" value="PreAuth" placeholder="" id="input-nestpay_transaction_type" class="form-control" <?php if($nestpay_transaction_type == "PreAuth" ) { echo "checked"; } ?> /> PreAuth

            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-order-status">Status uspešne transakcije</label>
            <div class="col-sm-10">
              <select name="nestpay_status_complete" id="input-order-status" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $nestpay_status_complete) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>



          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-order-fail">Status neuspele transakcije</label>
            <div class="col-sm-10">
              <select name="nestpay_status_failed" id="input-order-fail" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $nestpay_status_failed) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>


          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
            <div class="col-sm-10">
              <select name="nestpay_hosted_geo_zone_id" id="input-geo-zone" class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $nestpay_hosted_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="nestpay_status" id="input-status" class="form-control">
                <?php if ($nestpay_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>



        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>