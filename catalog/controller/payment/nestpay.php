<?php
class ControllerPaymentNestpay extends Controller {
	public function index() {
		$this->load->language('payment/nestpay');
		$this->load->model('checkout/order');
		$this->load->model('payment/nestpay');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$this->data["shopUrl"] = $this->config->get('config_url');

		$this->data['ORDER_ID'] = $this->session->data['order_id'];
		$this->data['NAME1'] = $order_info['payment_firstname'];
		$this->data['NAME2'] = $order_info['payment_lastname'];
		$this->data['ADDR1'] = $order_info['payment_address_1'];
		$this->data['ADDR2'] = $order_info['payment_address_2'];
		$this->data['CITY'] = $order_info['payment_city'];
		$this->data['STATE'] = $order_info['payment_zone'];
		$this->data['ZIPCODE'] = $order_info['payment_postcode'];
		$this->data['COUNTRY'] = $order_info['payment_country'];
		$this->data['PHONE'] = $order_info['telephone'];
		$this->data['EMAIL'] = $order_info['email'];


		$this->data["AMOUNT"] = $this->currency->format($order_info['total'], $order_info['currency_code'], false, false);
		$this->data['APPROVED_URL'] = $this->url->link('payment/nestpay/callback', '', true);
		$this->data['DECLINED_URL'] = $this->url->link('payment/nestpay/callback', '', true);

		$this->data['merchant'] = $this->config->get('nestpay_merchant_id');
		$this->data['store'] 	  = $this->config->get('nestpay_storekey');
		$this->data['URL'] 	  = $this->config->get('nestpay_url');
		$this->data['nestpay_transaction_type'] 	  = $this->config->get('nestpay_transaction_type');

		$this->data["owner_email"] = $this->config->get('config_email');
		$this->data["apikey"] 	 = $this->config->get('nestpay_api');

		$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['text_loading'] = $this->language->get('text_loading');

		//return $this->load->view('payment/nestpay', $data);

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/nestpay.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/nestpay.tpl';
		} else {
			$this->template = 'default/template/payment/nestpay.tpl';
		}
		$this->children = array(
'common/header',
'common/footer'
);

$this->response->setOutput($this->render($data));
		//$this->response->setOutput($this->render($data));
		//return $this->load->view($this->template, $data);



	}

	public function callback() {
		$this->load->language('payment/nestpay');

		$this->load->model('checkout/order');

		$this->load->model('payment/nestpay');

		$response_data = $this->request->post;





		if (isset( $response_data["ReturnOid"] )) {
			$order_info = $this->model_checkout_order->getOrder( $response_data["ReturnOid"] );



			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($order_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$subject = "Transaction status - Order #".$response_data["ReturnOid"];



			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));

			$html = "";
			ob_start();
			?>
	
				Your credit card transaction details - <?php echo $order_info['store_name'];?> : <br>
				Order ID:  <?php echo $response_data["ReturnOid"];?> <br>
				AuthCode:  <?php echo $response_data["AuthCode"];?> <br>
				OProcReturnCode:  <?php echo $response_data["ProcReturnCode"];?> <br>
				ErrMsg:  <?php echo $response_data["ErrMsg"];?> <br>
				TransId:  <?php echo $response_data["TransId"];?> <br>
				ReturnOid:  <?php echo $response_data["ReturnOid"];?> <br>


				<?php //print_r($response_data); ?>

			<?php
			$html = ob_get_clean();

			$mail->setHtml( $html );
			$mail->send();



			if ($response_data['Response'] == 'Approved') {



				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo( $this->config->get('nestpay_transaction_email') );
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
				$subject = "Transaction status - Approved - Order #".$response_data["ReturnOid"];



				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));

				$html = "";
				ob_start();
				?>
					
					New approved transaction! <br><br>
					Order ID:  <?php echo $response_data["ReturnOid"];?> <br>
					AuthCode:  <?php echo $response_data["AuthCode"];?> <br>
					OProcReturnCode:  <?php echo $response_data["ProcReturnCode"];?> <br>
					ErrMsg:  <?php echo $response_data["ErrMsg"];?> <br>
					TransId:  <?php echo $response_data["TransId"];?> <br>
					ReturnOid:  <?php echo $response_data["ReturnOid"];?> <br>


					<?php //print_r($response_data); ?>

				<?php
				$html = ob_get_clean();

				$mail->setHtml( $html );
				$mail->send();




				$this->model_checkout_order->addOrderHistory($response_data["ReturnOid"], $this->config->get('nestpay_status_complete'));

				$this->response->redirect($this->url->link('checkout/success', '', true));

			} else {

				$this->model_checkout_order->addOrderHistory($response_data["ReturnOid"], $this->config->get('nestpay_status_failed'));

				$this->session->data['error'] = "Transaction failed!";

				$this->response->redirect($this->url->link('checkout/checkout', '', true));

			}
		} else {
			$this->response->redirect($this->url->link('account/login', '', true));
		}
	}

	public function adminCallback() {
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($this->request->get));
	}
}