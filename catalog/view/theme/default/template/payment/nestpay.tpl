<?php 

$sConfirmationID = $ORDER_ID;
$orgClientId = $merchant;
$storeKey    = $store;
$link        = $URL;

$orgOid = $sConfirmationID;

 $orgCurrency = "978";

 $orgAmount = $AMOUNT;

 /// Temporary currency conversion - NestPay do not support multicurrency yet!


 $orgOkUrl = $APPROVED_URL;
 $orgFailUrl = $DECLINED_URL;
 $orgTransactionType = $nestpay_transaction_type;
 $orgInstallment = "";
 $orgRnd = date("his");


 $clientId = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgClientId));
 $oid = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgOid));
 $amount = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgAmount));
 $okUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgOkUrl));
 $failUrl = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgFailUrl));
 $transactionType = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgTransactionType));
 $installment = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgInstallment));
 $rnd = str_replace("|", "\\|", str_replace("\\", "\\\\", date("his")));
 $currency = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgCurrency));
 $storeKey = str_replace("|", "\\|", str_replace("\\", "\\\\", $storeKey));

 $plainText = $clientId . "|" . $oid . "|" . $amount . "|" . $okUrl . "|" . $failUrl . "|" .
 $transactionType . "|" . $installment . "|" . $rnd . "||||" . $currency . "|" . $storeKey;

 $hashValue = hash('sha512', $plainText);
 $hash = base64_encode (pack('H*',$hashValue)) ;
 $description = "";
 $xid = "";
$lang="";
$email="";
$userid="";



?>
<form action="<?php echo $link;?>" method="POST">
    <input type="hidden" name="shopurl" value="<?php echo $shopUrl; ?>">
    <input type="hidden" name="clientid" value="<?php echo $orgClientId ?>">
    <input type="hidden" name="amount" value="<?php echo $orgAmount ?>">
    <input type="hidden" name="oid" value="<?php echo $orgOid ?>">
    <input type="hidden" name="okurl" value="<?php echo $orgOkUrl ?>">
    <input type="hidden" name="failUrl" value="<?php echo $orgFailUrl ?>">
    <input type="hidden" name="TranType" value="<?php echo $orgTransactionType ?>">
    <input type="hidden" name="Instalment" value="<?php echo $orgInstallment ?>">
    <input type="hidden" name="currency" value="<?php echo $orgCurrency ?>">
    <input type="hidden" name="rnd" value="<?php echo $orgRnd ?>">
    <input type="hidden" name="hash" value="<?php echo $hash ?>">
    <input type="hidden" name="storetype" value="3D_PAY_HOSTING">
    <input type="hidden" name="hashAlgorithm" value="ver2">
    <input type="hidden" name="lang" value="en">
    <input type="hidden" name="refreshtime" value="2" />
    <div class="buttons">
        <div class="pull-right">
            <input type="submit" value="<?php echo $button_confirm; ?>" id="button-submit" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
        </div>
    </div>
</form>
<br>
<img src="https://spletnimoduli.si/modules/images/visa_verified-master_secure_code.png" alt="">